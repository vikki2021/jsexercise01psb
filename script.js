import './styles.scss';
import { bds } from './src/data';
// import $ from 'jquery';
import 'bootstrap';

console.log(bds);

const app = document.getElementById('app');

// $('body').append('jquery + bootstrap works!');

// every files in "static" folder can be used directly like that
// app.innerHTML += '<img src="images/kitten.jpg" style="width:100px;" />';

// document.getElementsByClassName('plus').addEventListener('click', (el) => {
//   // const short = document.getElementById(`short${id}`);
//   el.classList.add('text-hidden');
// });
// app.classList.add('card-columns');
let cardContainer = '<div class="card-columns">';
for (const bd of bds) {
  cardContainer += `
  <div class="card" style="width: 18rem;">
    <img src="images/${bd.image}" style="width:100px;" class="card-img-top" alt="...">
    <div class="card-body" id="${bd.id}">
        <h5 class="card-title">${bd.titre}</h5>
        <p class="card-text" id="short${bd.id}">${bd.resume.substring(0, 30)}</p>
        <p class="card-text text-hidden" id="long${bd.id}">${bd.resume}</p>
        <i class="fas fa-plus plus" id="${bd.id}" ></i>
        <i class="fas fa-minus minus" id="${bd.id}" ></i>
       <p class="card-text">ID:${bd.id}</p>
   <p class="card-text">Editor:${bd.editeur}</p>
  <p class="card-text">collection:${bd.collection}</p>
    <p class="card-text">series:${bd.serie}</p>

<p class="card-text">type:${bd.type}</p>
   <p class="card-text">state:${bd.etat}</p>
    <p  class="card-text">isbn:${bd.isbn}</p>
   <p class="card-text">year:${bd.annee_de_parution}</p>
    <p class="card-text">notome:${bd.no_tome}</p>
    <p class="card-text">price:${bd.prix}</p>
    <p class="card-text">themes:${bd.themes}</p>
    <p class="card-text">authors:${bd.auteurs}</p>
    <a href="#" class="btn btn-primary borrow">borrow</a>
     
      
    </div>
  </div>
  `;
}

cardContainer += '</div>';

const title = '<h1>Welcome to our library</h1>';
const basket = `
<div class="basket-popup">
  <div class="basket__title">
      <i class="fas fa-shopping-basket"></i>
      <span>View your basket (0)</span>
  </div>  
  <div class="basket__content">
  </div>
</div>
`;

app.innerHTML += title + basket + cardContainer;

// first create .text-hidden to display none in base_scss
// ,then click plus will show full text
// or click minus will hide description to only 30 alphabats
document.body.addEventListener('click', (e) => {
  console.log(e.target.id);
  if (e.target.matches('.plus')) {
    const short = document.getElementById(`short${e.target.id}`);
    const long = document.getElementById(`long${e.target.id}`);
    short.classList.add('text-hidden');
    long.classList.remove('text-hidden');
  } else if (e.target.matches('.minus')) {
    const short = document.getElementById(`short${e.target.id}`);
    const long = document.getElementById(`long${e.target.id}`);
    long.classList.add('text-hidden');
    short.classList.remove('text-hidden');
  }
});

// add book to basket
let count = 0;
const titleBasket = document.querySelector('.basket__title');
const contentBasket = document.querySelector('.basket__content');
document.body.addEventListener('click', (e) => {
  if (e.target.matches('.borrow')) {
    const { id } = e.target.parentNode;
    console.log(id);
    count += 1;
    titleBasket.innerHTML = `
        <i class="fas fa-shopping-basket"></i>
        <span>View your basket (${count})</span>
    `;
    contentBasket.innerHTML += `
        <div class="borrowedBook"><a href="#">${bds[id].titre} </a>
        </div>
    `;
  }
});
