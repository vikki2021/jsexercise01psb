**Exercises with Books(EN version)**
- ESLint is required to use!
- in an index.html, there is a div with the id "app" - forbidden to write anything else in the HTML
- in the javascript there is the array below
- using javascript, display all the books with their images and all their information except the summary
- for the summary, display only the first 30 characters and a "read more" button
- click on "read more" displays the rest of the summary
- each book also has a "borrow" button
- clicking on it adds the title of this book in a "basket" always visible in fixed position


**Only using the basics of JS (such as .innerHtml, no append etc.)**



# Parcel seed

> A very simple seed to start a modern Single Page App development in no time
> 
## how to use

First use:
- npm install
- npm start

Next use:
- npm start

Before final deploy:
- npm run build
- profit?

## ESlint

To fully enjoy ESlint, please
- install the official VSCode "ESlint" extension
- add this to your VSCode settings:
```json
  //...
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  //...
```

## JQuery and Bootsrap for quick POCs 

If needed, you can uncomment `bootstrap` + `jquery` imports in
- script.js
- style.scss
  
If you don't need them at all, you can remove thoses comments. Then in `package.json`:
  - remove deps
    - popper.js
    - bootstrap
    - jquery
